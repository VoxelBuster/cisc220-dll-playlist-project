/*
	Author: 	Galen Nare
	Date:   	2020-10-13
*/

#ifndef DNODE_HPP_
#define DNODE_HPP_

#include <stdlib.h>
#include "Song.hpp"

using namespace std;

class DNode {
	friend class DLL;
	
	Song *song;
	DNode *prev;
	DNode *next;
public:
	DNode();
	DNode(string s, string a, int lenmin, int lensec);
};



#endif /* DNODE_HPP_ */
